function zeroses(num,zeroses=2){
    for(num = ""+num; num.length<zeroses; num = "0"+num);
    return num;
}

class fecha{
    static A_days = ["Domingo", "Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado"];
    static A_month = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];

    constructor(){
        this.fecha = new Date( Date.now() );
    }

    getDay(){return this.fecha.getDay();}
    getMonth(){return this.fecha.getMonth();}
    getHour(){return this.fecha.getHours(   );}

    getDayName(){return fecha.A_days[this.getDay()];}
    getMonthName(){return fecha.A_month[this.getMonth()];}
    getYear( ) { return this.fecha.getUTCFullYear(); }

    addHours(houradd){
        this.fecha.setHours(this.fecha.getHours()+houradd);
    }

    addDay(dayadd){
        this.addHours(24*dayadd);
    }
    
    addMonth(monthadd){
        this.fecha.setMonth(this.fecha.getMonth()+monthadd);
    }

    toString(){
        return `${this.getDayName()} ${this.getDay()+13} de ${this.getMonthName()} del  ${this.getYear()}
        `;
    } 
}