class CookieManager
{
    static FLAG_LAST_COOKIE = -1;

    static c_config = { SameSite : "Lax" , defaultCookieValue: "null" };
    
    static parserCookie( ) 
    {
        let c = document.cookie;
            c = c.split(";");
        let d = [];

            for( var i = 0; i < c.length; i++ ) 
            {
                let r = c[i].split("=");
                if( r[0].length == 0 ) continue;

                if( r.length < 2 ) r[1] = this.c_config.defaultCookieValue;
                
                d.push({
                        name : r[0].replace(" " , ""),
                        value: r[1].replace(" ","")
                });
            }

            return d;
    }


    static getCookies( ) 
    {
        return this.parserCookie( ) ;	
    }

    static removeAll( ) 
    {
        let c = this.parserCookie( ) ;

        for( var i = 0; i < c.length; i++ )
        {
            this.removeCookieByName( c[i].name );
        }
    }

    static getLastCookie( ) 
    {
        let index  = this.parserCookie( ).length-1;
        let result = this.parserCookie()[index];
        
        return (result<0 ) ? null : {

                name : result.name,
                index: index
        }
    }

    static removeCookie( val ) 
    {
        if( val < 0 ) 
        {
            switch( val )
            {
                case this.FLAG_LAST_COOKIE:
                    val = this.getLastCookie( );
                break;
            }	
        }else
        {
            val = this.parserCookie()[val];
        }

        this.removeCookieByName( val.name );
    }

    static removeCookieByName( name )
    {
        let antes = new Date();
            antes.setMinutes( antes.getMinutes() - 1 );

            document.cookie = `${name}=; None ; SameSite=Lax ; Path=/; Expires=${antes};`
    }


    static isEmpty( ) 
    {
        return this.parserCookie().length == 0;
    }


    static renameCookie( original , nuevoNombre ) 
    {
        if( !this.existCookie( original ) )
        {
            console.error( `No existe la cookie. (${original})`);		
        }else if( this.existCookie( nuevoNombre ) ) 
        {
            console.error( `Ya existe una cookie con el mismo nombre. (${nuevoNombre}`);
        }else
        {
            this.setCookieByName( nuevoNombre , this.getCookieByName( original ) );
            this.removeCookieByName( original );
        }
    }

    static existCookie( cookie1 ) 
    {
        let a = this.parserCookie( ) ;
        for( var i = 0; i < a.length; i++ ) 
        {
            if( a[i].name == cookie1 ) return true;
        }

        return false;
    }

    static transferValueCookies ( cookie1 , cookie2 ) 
    {
        if( !this.existCookie( cookie1 ) ) 
        {
            console.error( `La cookie no existe (${cookie1})`);
        }else if( !this.existCookie( cookie2 ) ) 
        {
            console.error( `La cookie no existe (${cookie2}`);
        }else
        {
            this.setCookieByName( cookie2 , this.getCookieByName( cookie1 ) );;
        }
    }

    static createCookie( name , value = "" , datas = {} ) 
    {
        this.setCookieByName( name , value , datas );
    }

    static setCookieByName( name , value = "" , datas = {}  ) 
    {
        if( value == "" ||  value == null ) 
        value = this.c_config.defaultCookieValue;

        let config = { horas : 1 , SameSite : this.c_config.SameSite };

        if( datas.horas    != undefined ) config.horas = datas.horas;
        if( datas.SameSite != undefined ) config.horas = datas.horas;

        let fecha = new Date();
            fecha.setHours( fecha.getHours() + config.horas );

            document.cookie = `${name}=${value}; SameSite=${config.SameSite} ;  Path=/; Expires=${fecha};`
    
    }

    static getCookieByName( name , type = "value" ) 
    {
        
        let lista = CookieManager.parserCookie( );
        
        for( var i = 0; i < lista.length; i++ ) 
        {
            if(lista[i].name == name ) 
            return ( type == "value" ) ? lista[i].value : i;
        }

        return null;
    }

};

